function _merge_args(iargs,cargs)
    fi,ri... = iargs
    if fi isa Colon
        fc,rc... = cargs
        return (fc,_merge_args(ri,rc)...)
    else
        return (fi,_merge_args(ri,cargs)...)
    end
end

_merge_args(iargs::Tuple{},cargs) = cargs
_merge_args(iargs,cargs::Tuple{}) = iargs
_merge_args(iargs::Tuple{},cargs::Tuple{}) = ()

function Base.getindex(f::F,iargs...;ikwargs...) where {F<:Base.Callable}
    return (cargs...;ckwargs...) -> f(_merge_args(iargs,cargs)...;merge(ikwargs,ckwargs)...)
end
